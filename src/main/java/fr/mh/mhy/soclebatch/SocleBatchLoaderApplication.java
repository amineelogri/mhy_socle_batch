package fr.mh.mhy.soclebatch;

import fr.mh.mhy.soclebatch.dto.ConvertedInputData;
import fr.mh.mhy.soclebatch.dto.InputData;
import fr.mh.mhy.soclebatch.listener.BatchJobListener;
import fr.mh.mhy.soclebatch.processor.BatchProcessor;
import fr.mh.mhy.soclebatch.reader.BatchReader;
import fr.mh.mhy.soclebatch.utils.BatchStepSkipper;
import fr.mh.mhy.soclebatch.utils.Constants;
import fr.mh.mhy.soclebatch.writer.BatchWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.Resource;
import javax.sql.DataSource;


@SpringBootApplication
@EnableBatchProcessing
@PropertySources(@PropertySource(value = Constants.CONF_FILE, ignoreResourceNotFound = true))
public class SocleBatchLoaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocleBatchLoaderApplication.class, args);
    }

    @Resource
    Environment environment;

    @Autowired
    private DataSource dataSource;

    @Autowired
    PlatformTransactionManager transactionManager;

    @Bean
    public JobRepository jobRepositoryObj() throws Exception {
        JobRepositoryFactoryBean jobRepoFactory = new JobRepositoryFactoryBean();
        jobRepoFactory.setTransactionManager(transactionManager);
        jobRepoFactory.setDataSource(dataSource);
        return jobRepoFactory.getObject();
    }

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Bean
    public BatchReader batchReader() {
        return new BatchReader(environment);
    }

    @Bean
    public BatchProcessor batchProcessor() {
        return new BatchProcessor();
    }

    @Bean
    public BatchWriter batchWriter() {
        return new BatchWriter();
    }

    @Bean
    public BatchJobListener batchJobListener() {
        return new BatchJobListener();
    }

    @Bean
    public BatchStepSkipper batchStepSkipper() {
        return new BatchStepSkipper();
    }

    @Bean
    public Step batchStep() {
        return stepBuilderFactory.get("stepSocleBatchLoader").transactionManager(transactionManager)
                .<InputData, ConvertedInputData>chunk(1).reader(batchReader()).processor(batchProcessor())
                .writer(batchWriter()).faultTolerant().skipPolicy(batchStepSkipper()).build();
    }

    @Bean
    public Job jobStep() throws Exception {
        return jobBuilderFactory.get("jobSocleBatchLoader").repository(jobRepositoryObj()).incrementer(new RunIdIncrementer()).listener(batchJobListener())
                .flow(batchStep()).end().build();
    }

}

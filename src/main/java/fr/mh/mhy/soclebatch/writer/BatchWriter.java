package fr.mh.mhy.soclebatch.writer;


import fr.mh.mhy.soclebatch.dto.ConvertedInputData;
import fr.mh.mhy.soclebatch.model.Product;
import fr.mh.mhy.soclebatch.model.PurchaseDate;
import fr.mh.mhy.soclebatch.model.Purchaser;
import fr.mh.mhy.soclebatch.model.Supplier;
import fr.mh.mhy.soclebatch.repository.OrderRepository;
import fr.mh.mhy.soclebatch.repository.ProductRepository;
import fr.mh.mhy.soclebatch.repository.PurchaseDateRepository;
import fr.mh.mhy.soclebatch.repository.PurchaserRepository;
import fr.mh.mhy.soclebatch.repository.SupplierRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BatchWriter implements ItemWriter<ConvertedInputData> {

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private PurchaserRepository purchaserRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PurchaseDateRepository purchaseDateRepository;

    @Autowired
    private OrderRepository commandRepository;

    @Override
    public void write(List<? extends ConvertedInputData> items) throws Exception {
        items.stream().forEach(item -> {
            Supplier supplier = null;
            Purchaser purchaser = null;
            Product product = null;
            PurchaseDate purchaseDate = null;
            if (item.getOrder().getSupplierId() == null) {
                supplier = supplierRepository.save(item.getSupplier());
                item.getOrder().setSupplierId(supplier.getId());
            }
            if (item.getOrder().getPurchaserId() == null) {
                purchaser = purchaserRepository.save(item.getPurchaser());
                item.getOrder().setPurchaserId(purchaser.getId());
            }
            if (item.getOrder().getProductId() == null) {
                product = productRepository.save(item.getProduct());
                item.getOrder().setProductId(product.getId());
            }
            if (item.getOrder().getDateId() == null) {
                purchaseDate = purchaseDateRepository.save(item.getPurchaseDate());
                item.getOrder().setDateId(purchaseDate.getId());
            }
            commandRepository.save(item.getOrder());
        });
    }

}

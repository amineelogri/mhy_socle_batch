package fr.mh.mhy.soclebatch.dto;


import fr.mh.mhy.soclebatch.model.Order;
import fr.mh.mhy.soclebatch.model.Product;
import fr.mh.mhy.soclebatch.model.PurchaseDate;
import fr.mh.mhy.soclebatch.model.Purchaser;
import fr.mh.mhy.soclebatch.model.Supplier;
import lombok.Data;

@Data
public class ConvertedInputData {

    private Supplier supplier;

    private Purchaser purchaser;

    private Product product;

    private PurchaseDate purchaseDate;

    private Order order;
}

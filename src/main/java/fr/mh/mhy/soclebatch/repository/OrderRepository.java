package fr.mh.mhy.soclebatch.repository;


import org.springframework.data.repository.CrudRepository;
import fr.mh.mhy.soclebatch.model.Order;


public interface OrderRepository extends CrudRepository<Order, Long> {

}

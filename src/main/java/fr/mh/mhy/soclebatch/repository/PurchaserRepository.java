package fr.mh.mhy.soclebatch.repository;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import fr.mh.mhy.soclebatch.model.Purchaser;

public interface PurchaserRepository extends CrudRepository<Purchaser, Long> {

	@Query("select * from DIM_EMPLOYEE_PURCHASER where EMAIL = :email")
	Purchaser findByEmail(@Param("email") String email);

}

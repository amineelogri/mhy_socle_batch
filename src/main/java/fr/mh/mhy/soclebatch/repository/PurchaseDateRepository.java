package fr.mh.mhy.soclebatch.repository;

import java.util.Date;

import fr.mh.mhy.soclebatch.model.PurchaseDate;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;



public interface PurchaseDateRepository extends CrudRepository<PurchaseDate, Long> {

	@Query("select * from DIM_DATE where DATE_TIME = :date ")
	PurchaseDate findByDate(@Param("date") Date date);

}

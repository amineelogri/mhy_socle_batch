package fr.mh.mhy.soclebatch;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@SpringBatchTest
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:_100-test.properties")
class SocleBatchApplicationTests {

	@Autowired
	JobLauncherTestUtils jobLaucherTestUtils;



	@Test
	public void testSocleBatchLoader() throws Exception {
		JobExecution jobExecution = jobLaucherTestUtils.launchJob();

		Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
	}

}
